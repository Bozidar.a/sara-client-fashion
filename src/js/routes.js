
import HomePage from '../pages/home.vue';
import AboutPage from '../pages/about.vue';

import RequestAndLoad from '../pages/request-and-load.vue';
import NotFoundPage from '../pages/404.vue';

var routes = [
  {
    path: '/',
    component: HomePage,
  },
  {
    path: '/about/',
    component: AboutPage,
  },
  {
    path: '/products',

    async: function (routeTo, routeFrom, resolve, reject) {

      // Router instance
      var router = this

      // App instance
      var app = router.app

      // Show Preloader
      app.preloader.show()

      axios.get('https://picsum.photos/v2/list?page=1&limit=100').then(response => {
        
        let products = response.data

        // Hide Preloader
        app.preloader.hide();

        // Resolve route to load page
        resolve(
          {
            component: RequestAndLoad,
          },
          {
            context: {
              products: products,
            }
          }
        );

      })
    },
  },
  {
    path: '(.*)',
    component: NotFoundPage,
  },
];

export default routes;
